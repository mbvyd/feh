#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=C:\Program Files (x86)\AutoIt3\Icons\au3.ico
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_ProductName=Frog eTXT helper
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#cs ----------------------------------------------------------------------------

	AutoIt Version: 3.3.14.5
	Author:         https://gitlab.com/intraum


	регулярка для урла https://regexr.com/3g760 (сильно её упростил)


	комментарии:

	  * дубли по контенту НЕ ищутся:

		  - это не имеет смысла потому что это проще и лучше может сделать сам etxt
			с пом. локального поиска (в т.ч. найдёт дубли разной степени);

		  - полных дублей по логике должно быть крайне мало, и если файлов с контентом сотни,
		    то сравнение больших текстовых кусков - пустая трата времени;

		  - проверка etxt очень долгая, и несколько дополнительных проверок дублей сэкономят
			мизерное количество времени (по сравнению с продолжительностью всей операции);
			а лишние расходы на антикапчу в таком случае должны быть крошечными;


	  * некоторые отчёты eTXT удаляются без участия пользователя, потому что они бесполезны:

		  - имя файла заканчивается на _undef.html;
		  - контент содержит информацию, что отсутствует исходный текст;


	  * etxt создаёт следующие файлы:

		  - полностью выполнена проверка;
		  - проверка прервана, исходный текст есть (нельзя отличить от предыдущего);
		  - проверка прервана, етхт ставит метку undef в имени файла;
		  - проверка прервана, исходного текста нет (нельзя отличить от предыдущего по содержимому);

		  никак не отличить неполностью отработанный файл от полностью проработанного,
		  это по-хорошему надо делать на уровне событий етхт (что недоступно),
		  в т.ч. удаления неполных отчётов;


	  * отсев отработанного:

		  - упаковка в архив, а не перемещение файлов, т.к. етхт работает с папкой рекурсивно -
		    то есть, если переместить что-то в подпапку, етхт возьмёт это в работу
			при проверке родительской папки;

		  - файлы, созданные самим етхт упаковывать не надо, т.к. они исключаются самим etxt,
		    а упаковка добавит необходимость искать и распаковывать архивы с исходниками -
			на этапе формирования отчёта;


#ce ----------------------------------------------------------------------------

#include <Array.au3>
#include <File.au3>
; для GUI, от ISN
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <GuiButton.au3>
#include <EditConstants.au3>
; для редактирования значений контролов
#include <GuiEdit.au3>
; для кодов для MsgBox
#include <MsgBoxConstants.au3>
#include "zavisimosti\FileAux.au3"
#include "zavisimosti\FileTimes.au3"
#include "zavisimosti\ArraysCompare.au3"
#include "zavisimosti\StringAux.au3"
#include "zavisimosti\GUIAux.au3"
; для упаковки отработанных исходников
#include "zavisimosti\_Zip.au3"


#Region ; init ---------

; используемый метод управлением пользовательскими интерфейсами
Opt("GUIOnEventMode", 1)

; интерфейсы
Global $g_sWinTitle, $g_sExtractors, $g_sCSVpath, $g_sDirPath

; состояние чекбокса ограничения символов, соответствующее числовое значение
; и номер радиобатона для удаления *.html, который должен быть выбран
Global $g_iCharsLimitOn, $g_iCharsLimit, $g_iHtmlRadio

; объекты для хранения контролов интерфейсов
Global $g_oGUImain, $g_oGUIslicing, $g_oGUIreporting, $g_oGUIspliting

; названия ключей настроек из файла настроек
Global $g_sConfigFilePath, $g_sConfigSection, $g_sKeyDir, $g_sKeyCsv, _
		$g_sKeyExtractors, $g_sKeyCharsLimitOn, $g_sKeyCharsLimit, $g_sKeyHtmlRadio

; массив сопоставления имён файлов и урлов (нужен для всех режимов работы)
Global $g_aMatching
Global $g_sFileMatchingName, $g_sHTMLfilePath


init()

Func init()

	setVals()

	; основной интерфейс
	createGUImain()
	GUISetState(@SW_SHOW, $g_oGUImain.Item('hGUI'))

EndFunc   ;==>init


Func setVals()

	$g_sWinTitle = "Frog eTXT helper"
	$g_sFileMatchingName = 'feh_matching_table.txt'

	$g_sConfigFilePath = @ScriptDir & "\" & "config.ini"
	$g_sConfigSection = "general"
	$g_sKeyDir = "dir"
	$g_sKeyCsv = 'csv'
	$g_sKeyExtractors = "extractors"
	$g_sKeyCharsLimitOn = 'charsLimitOn'
	$g_sKeyCharsLimit = 'charsLimit'
	$g_sKeyHtmlRadio = 'radioHtml'

	If Not FileExists($g_sConfigFilePath) Then
		$g_iCharsLimitOn = 0
		; прочие значения интерфейсов не задаём; а радиобатон удаления хтмл
		; будет выбран нужный автоматически
		Return
	EndIf

	; пробуем взять данные из файла настроек
	$g_sDirPath = IniRead($g_sConfigFilePath, $g_sConfigSection, $g_sKeyDir, "")
	$g_sCSVpath = IniRead($g_sConfigFilePath, $g_sConfigSection, $g_sKeyCsv, "")
	$g_sExtractors = IniRead($g_sConfigFilePath, $g_sConfigSection, $g_sKeyExtractors, "")
	$g_iCharsLimit = IniRead($g_sConfigFilePath, $g_sConfigSection, $g_sKeyCharsLimit, "")

	; тут обязательно числовой тип данных (чтобы далее работали выражения = ? : )
	$g_iCharsLimitOn = Number(IniRead($g_sConfigFilePath, $g_sConfigSection, $g_sKeyCharsLimitOn, "0"))
	$g_iHtmlRadio = Number(IniRead($g_sConfigFilePath, $g_sConfigSection, $g_sKeyHtmlRadio, "1"))

	; выключаем опцию, если указано бредовое значение (пока $g_iCharsLimit - строка)
	; (такая комбинация в настройках возможна)
	If $g_iCharsLimitOn = 1 Then
		If StringStripWS($g_iCharsLimit, 8) = "" Or Number($g_iCharsLimit) < 10 Then
			$g_iCharsLimitOn = 0
		EndIf
	EndIf
EndFunc   ;==>setVals


Func createGUImain()
	Local $hGUI

	$hGUI = GUICreate($g_sWinTitle, 265, 311, -1, -1, -1, -1)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit", $hGUI)
	GUICtrlCreateButton("Разрезание на файлы", 40, 27, 188, 67, -1, -1)
	GUICtrlSetOnEvent(-1, "invokeSlicing")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateButton("Отсев отработанного", 40, 120, 188, 67, -1, -1)
	GUICtrlSetOnEvent(-1, "invokeSpliting")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateButton("Формирование отчёта", 40, 214, 188, 67, -1, -1)
	GUICtrlSetOnEvent(-1, "invokeReporting")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")

	$g_oGUImain = ObjCreate("Scripting.Dictionary")
	$g_oGUImain.Add("hGUI", $hGUI)
EndFunc   ;==>createGUImain


Func createGUIslicing()
	Local $hGUI
	Local $hInputCharsLimit
	Local $hInputExtractors
	Local $hChkboxCharsLimit
	Local $hInputFile
	Local $hInputDir

	$hGUI = GUICreate($g_sWinTitle & ' - разбор CSV на файлы', 518, 349, -1, -1, -1, $WS_EX_ACCEPTFILES)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit", $hGUI)
	GUISetOnEvent($GUI_EVENT_DROPPED, "_GUIonFileDrop", $hGUI)
	$hInputCharsLimit = GUICtrlCreateInput($g_iCharsLimit, 179, 55, 67, 22, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $g_iCharsLimitOn = 1 ? $GUI_ENABLE : $GUI_DISABLE))
	GUICtrlCreateLabel("Экстракторы Лягушки:", 20, 20, 149, 21, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	GUICtrlSetTip(-1, "Названия или порядковые номера (в интерфейсе Лягушки), через запятую")
	$hInputExtractors = GUICtrlCreateInput($g_sExtractors, 169, 17, 327, 22, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$hChkboxCharsLimit = GUICtrlCreateCheckbox("Фильтр по символам:", 20, 55, 154, 20, -1, -1)
	GUICtrlSetOnEvent(-1, "toggleCharsLimitInput")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Если размер текста, собранного из экстракторов, меньше заданного порога, то соответствующий URL не берётся в работу")
	ControlCommand($hGUI, "", $hChkboxCharsLimit, $g_iCharsLimitOn = 1 ? "Check" : "UnCheck", "")
	GUICtrlCreateGroup("CSV файл", 20, 98, 476, 73, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	$hInputFile = GUICtrlCreateInput($g_sCSVpath, 41, 126, 394, 23, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_ENABLE, $GUI_DROPACCEPTED))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, 'Экспорт Extraction из Лягушки')
	GUICtrlCreateButton("...", 447, 126, 29, 23, -1, -1)
	GUICtrlSetOnEvent(-1, "selectFileGUIslicing")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateGroup("Папка", 20, 186, 476, 76, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	$hInputDir = GUICtrlCreateInput($g_sDirPath, 41, 215, 394, 23, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_ENABLE, $GUI_DROPACCEPTED))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, 'Куда сохраняем результирующие файлы')
	GUICtrlCreateButton("...", 447, 215, 29, 23, -1, -1)
	GUICtrlSetOnEvent(-1, "selectDirGUIslicing")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateButton("Старт", 227, 284, 100, 36, -1, -1)
	GUICtrlSetOnEvent(-1, "goGUIslicing")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")

	$g_oGUIslicing = ObjCreate("Scripting.Dictionary")
	$g_oGUIslicing.Add("hGUI", $hGUI)
	$g_oGUIslicing.Add("hInputCharsLimit", $hInputCharsLimit)
	$g_oGUIslicing.Add("hInputExtractors", $hInputExtractors)
	$g_oGUIslicing.Add("hChkboxCharsLimit", $hChkboxCharsLimit)
	$g_oGUIslicing.Add("hInputFile", $hInputFile)
	$g_oGUIslicing.Add("hInputDir", $hInputDir)
EndFunc   ;==>createGUIslicing


Func createGUIreporting()
	Local $hGUI
	Local $hInputDir

	$hGUI = GUICreate($g_sWinTitle & ' - отчёт из HTML', 516, 172, -1, -1, -1, $WS_EX_ACCEPTFILES)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit", $hGUI)
	GUISetOnEvent($GUI_EVENT_DROPPED, "_GUIonFileDrop", $hGUI)
	GUICtrlCreateGroup("Папка", 20, 15, 474, 76, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	$hInputDir = GUICtrlCreateInput($g_sDirPath, 40, 44, 395, 23, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_ENABLE, $GUI_DROPACCEPTED))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, 'Где находятся файлы-отчёты eTXT')
	GUICtrlCreateButton("...", 446, 44, 29, 23, -1, -1)
	GUICtrlSetOnEvent(-1, "selectDirGUIreporting")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateButton("Старт", 220, 111, 100, 36, -1, -1)
	GUICtrlSetOnEvent(-1, "goGUIreporting")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")

	$g_oGUIreporting = ObjCreate("Scripting.Dictionary")
	$g_oGUIreporting.Add("hGUI", $hGUI)
	$g_oGUIreporting.Add("hInputDir", $hInputDir)

EndFunc   ;==>createGUIreporting


Func createGUIspliting()
	Local $hGUI
	Local $hInputDir
	Local $hRadioLast
	Local $hRadioAll
	Local $hRadioNone

	$hGUI = GUICreate($g_sWinTitle & ' - чистка', 516, 262, -1, -1, -1, $WS_EX_ACCEPTFILES)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit", $hGUI)
	GUISetOnEvent($GUI_EVENT_DROPPED, "_GUIonFileDrop", $hGUI)
	GUICtrlCreateGroup("Папка", 20, 15, 474, 76, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	$hInputDir = GUICtrlCreateInput($g_sDirPath, 40, 44, 395, 23, -1, $WS_EX_CLIENTEDGE)
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_ENABLE, $GUI_DROPACCEPTED))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, 'Где находятся исходники и файлы отчётов eTXT')
	GUICtrlCreateButton("...", 446, 44, 29, 23, -1, -1)
	GUICtrlSetOnEvent(-1, "selectDirGUIspliting")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlCreateGroup("Удаление *.html", 20, 109, 200, 128, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	$hRadioLast = GUICtrlCreateRadio("Последний", 40, 138, 106, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Будет удалён самый новый")
	ControlCommand($hGUI, "", $hRadioLast, $g_iHtmlRadio = 1 ? "Check" : "UnCheck", "")
	$hRadioAll = GUICtrlCreateRadio("Все", 40, 166, 106, 20, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Удалить все файлы отчётов eTXT")
	ControlCommand($hGUI, "", $hRadioAll, $g_iHtmlRadio = 2 ? "Check" : "UnCheck", "")
	$hRadioNone = GUICtrlCreateRadio("Мусорные", 40, 195, 105, 20, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Ничего не будет удалено, кроме файлов с отсутствующими данными")
	ControlCommand($hGUI, "", $hRadioNone, $g_iHtmlRadio = 3 ? "Check" : "UnCheck", "")
	GUICtrlCreateButton("Старт", 335, 156, 100, 30, -1, -1)
	GUICtrlSetOnEvent(-1, "goGUIspliting")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")

	$g_oGUIspliting = ObjCreate("Scripting.Dictionary")
	$g_oGUIspliting.Add("hGUI", $hGUI)
	$g_oGUIspliting.Add("hInputDir", $hInputDir)
	$g_oGUIspliting.Add("hRadioLast", $hRadioLast)
	$g_oGUIspliting.Add("hRadioAll", $hRadioAll)
	$g_oGUIspliting.Add("hRadioNone", $hRadioNone)

EndFunc   ;==>createGUIspliting


Func callGUI($iGUI)
	Local $hWorkGUI
	Switch $iGUI
		;интерфейс разрезки на файлы
		Case 1
			createGUIslicing()
			$hWorkGUI = $g_oGUIslicing.Item("hGUI")

			; интерфейс отсева проверенных файлов
		Case 2
			createGUIspliting()
			$hWorkGUI = $g_oGUIspliting.Item("hGUI")

			;интерфейс составления отчёта
		Case 3
			createGUIreporting()
			$hWorkGUI = $g_oGUIreporting.Item("hGUI")
	EndSwitch

	;GUISetState(@SW_HIDE, $g_oGUImain.Item("hGUI"))
	GUIDelete($g_oGUImain.Item("hGUI"))
	$g_oGUImain = 0
	GUISetState(@SW_SHOW, $hWorkGUI)

EndFunc   ;==>callGUI

Func invokeSlicing()
	callGUI(1)
EndFunc   ;==>invokeSlicing

Func invokespliting()
	callGUI(2)
EndFunc   ;==>invokespliting

Func invokeReporting()
	callGUI(3)
EndFunc   ;==>invokeReporting

#EndRegion ; init ---------


; простой GUI ( https://www.autoitscript.com/autoit3/docs/guiref/GUIRef_OnEventMode.htm )
While 1
	Sleep(50)
WEnd


#Region ; базовые события интерфейсов --------

Func _Exit()
	Exit
EndFunc   ;==>_Exit


; по проставлению/снятию галки об ограничении количества символов
Func toggleCharsLimitInput()
	Local $hControl = $g_oGUIslicing.Item("hInputCharsLimit")
	Local $iState = GUICtrlGetState($hControl)

	If $iState = 144 Then
		GUICtrlSetState($hControl, $GUI_ENABLE)
	Else
		GUICtrlSetState($hControl, $GUI_DISABLE)
	EndIf
EndFunc   ;==>toggleCharsLimitInput


; при перетаскивании файла в поле
Func _GUIonFileDrop()
	GUICtrlSetData(@GUI_DropId, @GUI_DragFile)
EndFunc   ;==>_GUIonFileDrop

#EndRegion ; базовые события интерфейсов --------


#Region ; выбор папок и файлов --------

;выбор файла
Func selectFile($iInterface)
	Local $sTitleSuffix, $sFileTypes, $hControl, $hGUI

	Switch $iInterface
		;интерфейс распила на файлы
		Case 1
			$sTitleSuffix = " - выберите файл"
			$sFileTypes = "CSV (*.csv)|TXT (*.txt)"
			$hControl = $g_oGUIslicing.Item("hInputFile")
			$hGUI = $g_oGUIslicing.Item("hGUI")

			;оставляю в таком виде, чтобы если в какой-то интерфейс добавятся поля выбора файла,
			;то чтобы можно было их обработать отдельными кейсами здесь
	EndSwitch

	; папка по умолчанию
	Local $sCurrDir = _GetDirPathFromFilePath(GUICtrlRead($hControl))
	Local $sSelectedFile = FileOpenDialog($g_sWinTitle & $sTitleSuffix, $sCurrDir, $sFileTypes, 0, "", $hGUI)

	;если файл был выбран (иначе не трогаем, и остаётся прежнее значение)
	If Not @error Then _GUICtrlEdit_SetText($hControl, $sSelectedFile)

EndFunc   ;==>selectFile


;для выбора CSV в интерфейсе разрезателя
Func selectFileGUIslicing()
	selectFile(1)
EndFunc   ;==>selectFileGUIslicing


;выбор папки
Func selectDir($iInterface)
	Local $hControl, $hGUI, $sTitleSuffix = " - выберите папку"

	Switch $iInterface
		;интерфейс распила на файлы
		Case 1
			$hControl = $g_oGUIslicing.Item("hInputDir")
			$hGUI = $g_oGUIslicing.Item("hGUI")

		Case 2
			$hControl = $g_oGUIspliting.Item("hInputDir")
			$hGUI = $g_oGUIspliting.Item("hGUI")

			;интерфейс отчётности
		Case 3
			$hControl = $g_oGUIreporting.Item("hInputDir")
			$hGUI = $g_oGUIreporting.Item("hGUI")
	EndSwitch

	;папка по умолчанию
	Local $sCurrDir = GUICtrlRead($hControl)

	Local $sSelectedFolder = FileSelectFolder($g_sWinTitle & $sTitleSuffix, $sCurrDir, 0, "", $hGUI)

	;если папка была выбрана (иначе не трогаем, и остаётся прежнее значение)
	If Not @error Then _GUICtrlEdit_SetText($hControl, $sSelectedFolder)

EndFunc   ;==>selectDir


; для выбора папки для файлов, которые будут созданы при разборе CSV - в интерфейсе разрезателя
Func selectDirGUIslicing()
	selectDir(1)
EndFunc   ;==>selectDirGUIslicing


; для выбора папки в интерфейсе отсева
Func selectDirGUIspliting()
	selectDir(2)
EndFunc   ;==>selectDirGUIspliting


; для выбора папки с файлами отчётов eTXT в интерфейсе подготовки отчёта
Func selectDirGUIreporting()
	selectDir(3)
EndFunc   ;==>selectDirGUIreporting

#EndRegion ; выбор папок и файлов --------


#Region ; запуски --------

;по запуску работы по распилу на файлы
Func goGUIslicing()

	Local $hGUI = $g_oGUIslicing.Item("hGUI")

	prepToSlice($hGUI)
	If @error Then Return

	GUISetState(@SW_HIDE, $hGUI)

	procCSV($hGUI)
	If Not @error Then
		MsgBox($MB_ICONINFORMATION, $g_sWinTitle, "Работа завершена", 0, $hGUI)
	EndIf

	GUISetState(@SW_SHOW, $hGUI)

EndFunc   ;==>goGUIslicing


Func goGUIspliting()
	Local $hGUI = $g_oGUIspliting.Item("hGUI")

	prepToSplit($hGUI)
	If @error Then Return

	GUISetState(@SW_HIDE, $hGUI)

	splitFiles($hGUI)
	If Not @error Then
		MsgBox($MB_ICONINFORMATION, $g_sWinTitle, _
				"Работа по очистке от лишних html и упаковке отработанных txt завершена " _
				 & "(при этом, могло не быть никаких изменений, если они не требовались)" _
				, 0, $hGUI)
	EndIf

	GUISetState(@SW_SHOW, $hGUI)

EndFunc   ;==>goGUIspliting


;по запуску работы по созданию отчёта
Func goGUIreporting()

	Local $hGUI = $g_oGUIreporting.Item("hGUI")

	prepToFormReport($hGUI)
	If @error Then Return

	GUISetState(@SW_HIDE, $hGUI)

	formReport($hGUI)
	If Not @error Then

		Local $iAns = MsgBox($MB_ICONINFORMATION + $MB_OKCANCEL, $g_sWinTitle, "Файл отчёта index.html подготовлен и находится в папке " & $g_sDirPath & @CRLF & "Чтобы открыть его сейчас, нажмите ОК", 0, $hGUI)
		If $iAns = 1 Then ShellExecute($g_sHTMLfilePath)
	EndIf

	GUISetState(@SW_SHOW, $hGUI)

EndFunc   ;==>goGUIreporting

#EndRegion ; запуски --------


#Region ; сбор данных из интерфейсов --------

; получение значения контрола и сохранение его в файл настроек при необходимости
Func getSaveCtrlVal($hControl, $vLastCtrlVal, $sConfigKey, $iChkboxRadio = 0)

	Local $vCtrlVal

	If Not $iChkboxRadio Then
		$vCtrlVal = GUICtrlRead($hControl)
	Else
		$vCtrlVal = _GUICtrlChkboxRadRead($hControl)
	EndIf

	; нет смысла переписывать настройки, если значение оттуда
	; совпадает с текущим значением контрола
	If $vCtrlVal = $vLastCtrlVal Then Return $vCtrlVal

	IniWrite($g_sConfigFilePath, $g_sConfigSection, $sConfigKey, $vCtrlVal)
	Return $vCtrlVal
EndFunc   ;==>getSaveCtrlVal


; получить значение папки и сохранить в настройки при необходимости
Func getDir($hControl)
	Return getSaveCtrlVal($hControl, $g_sDirPath, $g_sKeyDir)
EndFunc   ;==>getDir


; обновление данных для порезки значениями из интерфейса, проверки
Func prepToSlice($hGUI)

	$g_sCSVpath = getSaveCtrlVal($g_oGUIslicing.Item("hInputFile"), $g_sCSVpath, $g_sKeyCsv)

	If Not FileExists($g_sCSVpath) Then
		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Файл не существует", 0, $hGUI)
		Return SetError(1)
	EndIf

	If _fileInUse($g_sCSVpath) Then
		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Похоже, файл занят другим процессом", 0, $hGUI)
		Return SetError(2)
	EndIf

	$g_sDirPath = getDir($g_oGUIslicing.Item("hInputDir"))
	uiDirCheck($hGUI)
	If @error Then Return SetError(3)

	$g_sExtractors = getSaveCtrlVal($g_oGUIslicing.Item("hInputExtractors"), $g_sExtractors, $g_sKeyExtractors)
	$g_iCharsLimitOn = getSaveCtrlVal($g_oGUIslicing.Item("hChkboxCharsLimit"), $g_iCharsLimitOn, $g_sKeyCharsLimitOn, 1)

	; если галка ограничения символов не отмечена, игнорируем поле;
	; сразу приводим к числовым значениям (т.к. именно они будут использоваться в дальнейшем)
	If $g_iCharsLimitOn Then

		$g_iCharsLimit = getSaveCtrlVal($g_oGUIslicing.Item("hInputCharsLimit"), $g_iCharsLimit, $g_sKeyCharsLimit)
		$g_iCharsLimit = Number($g_iCharsLimit)
	Else
		$g_iCharsLimit = 0
	EndIf
EndFunc   ;==>prepToSlice


; обновление данных для чистки значениями из интерфейса, проверки
Func prepToSplit($hGUI)

	$g_sDirPath = getDir($g_oGUIspliting.Item("hInputDir"))

	uiDirCheck($hGUI)
	If @error Then Return SetError(1)

	; какую цифру (1/2/3) писать в файл настроек - в зависимости от выбранного радиобатона
	Local $iRadio
	Select
		Case _GUICtrlChkboxRadRead($g_oGUIspliting.Item("hRadioLast"))
			$iRadio = 1
		Case _GUICtrlChkboxRadRead($g_oGUIspliting.Item("hRadioAll"))
			$iRadio = 2
		Case _GUICtrlChkboxRadRead($g_oGUIspliting.Item("hRadioNone"))
			$iRadio = 3
	EndSelect

	; переписываем в настройках только если значение отличается
	If $iRadio <> $g_iHtmlRadio Then
		IniWrite($g_sConfigFilePath, $g_sConfigSection, $g_sKeyHtmlRadio, $iRadio)
		$g_iHtmlRadio = $iRadio
	EndIf

EndFunc   ;==>prepToSplit


; обновление данных для отчёта значениями из интерфейса, проверки
Func prepToFormReport($hGUI)

	$g_sDirPath = getDir($g_oGUIreporting.Item("hInputDir"))

	uiDirCheck($hGUI)
	If @error Then Return SetError(1)

	$g_sHTMLfilePath = $g_sDirPath & "\index.html"

	Local $bFileExists = FileExists($g_sHTMLfilePath)
	Local $bFileInUse = _fileInUse($g_sHTMLfilePath)

	Select
		Case Not $bFileExists
			; избежать проверки других условий для экономии ресурсов

		Case $bFileExists And Not $bFileInUse

			Local $iContinue = MsgBox($MB_ICONQUESTION + $MB_YESNO + $MB_DEFBUTTON2, $g_sWinTitle, "Файл отчёта (index.html) уже существует в указанной папке. Если продолжить, он будет перезаписан. Продолжить?", 0, $hGUI)
			If $iContinue <> 6 Then Return SetError(2)

			; для html это лишнее, вроде
		Case $bFileExists And $bFileInUse
			MsgBox($MB_ICONWARNING, $g_sWinTitle, "Похоже, файл занят другим процессом", 0, $hGUI)
			Return SetError(3)
	EndSelect
EndFunc   ;==>prepToFormReport


#EndRegion ; сбор данных из интерфейсов --------


#Region ; вспомогательное --------

Func uiDirCheck($hGUI)

	If _dirWritable($g_sDirPath) Then Return

	MsgBox($MB_ICONWARNING, $g_sWinTitle, "Папка не существует или не доступна для записи (возможно не хватает прав администратора)", 0, $hGUI)
	Return SetError(1)
EndFunc   ;==>uiDirCheck


; разбираем пути к файлам-отчётам, сформированным eTXT;
; берём все html файлы, кроме исключений - через вертикальную черту;
; (етхт создаёт _undef.html файлы, когда проверка была неудачна,
; также, проверяет и создаёт отчёт по таблице сопоставления)
Func readEtxtReports(ByRef $aArray, $hGUI = Null, $bExcludeUndefined = True, $bSilentRun = False)

	Local $sMask = "*.html|index.html;" & $g_sFileMatchingName & "*"
	If $bExcludeUndefined Then $sMask &= ';' & '*_undef.html'

	$aArray = _FileListToArrayRec( _
			$g_sDirPath, $sMask, $FLTAR_FILES, Default, Default, $FLTAR_FULLPATH _
			)

	If $bSilentRun Then Return

	If @extended = 9 Then
		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Не найдено ни одного HTML файла отчёта eTXT", 0, $hGUI)
		Return SetError(1, 1)

	ElseIf @error Then
		MsgBox($MB_ICONERROR, $g_sWinTitle, "Не удалось прочитать список HTML файлов в папке", 0, $hGUI)
		Return SetError(1)
	EndIf
EndFunc   ;==>readEtxtReports

#EndRegion ; вспомогательное --------


#Region ; разрезка на файлы --------

; считываем CSV и пишем данные в файлы;
Func procCSV($hGUI)

	;разбор значений экстракторов, указанных пользователем
	Local $aInputs = StringSplit($g_sExtractors, ",")

	Local $aFile
	getFileData($aFile)
	If @error Then
		MsgBox($MB_ICONERROR, $g_sWinTitle, "Не получилось прочитать файл. Попробуйте запустить процесс нарезки на файлы заново" & @CRLF & @CRLF & "(код ошибки " & @error & ")", 0, $hGUI)
		Return SetError(1)
	EndIf

	; файл должен содержать минимум 2 строки
	If $aFile[0] < 2 Then
		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Похоже, CSV не является корректным экспортом Лягушки", 0, $hGUI)
		Return SetError(2)
	EndIf

	; (ре-)инициализация массива сопоставления
	initArrMatching($aFile[0])
	If @error Then
		MsgBox($MB_ICONERROR, $g_sWinTitle, "Ошибка при формировании таблицы сопоставления, возможно, файл экспорта Лягушки некорректен.", 0, $hGUI)
		Return SetError(3)
	EndIf

	; формируем массив с порядковыми номерами столбцов, из которых нужно выдрать данные
	Local $aiCols[0]
	getColsCounts($aInputs, $aFile, $aiCols)

	; если даже один экстрактор не получилось привязать к CSV,
	; завершаем, т.к. в данном случае пользователь получит не те результаты,
	; которые ожидает
	If @error Then
		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Не найден хотя бы 1 экстрактор, или некорректная запись в поле экстракторов, или CSV не является корректным экспортом Лягушки", 0, $hGUI)
		Return SetError(4)
	EndIf

	; проходим КСВ построчно, пропуская первую строку (заголовок);
	; $bWasSaveEvent - переменная статуса о том, был ли сохранён хотя бы 1 файл;
	; $iRowMatching - номер ряда в массиве сопоставления
	Local $asRow, $bWasSaveEvent = False, $iRowMatching = 0

	For $i = 2 To $aFile[0]

		; разбираем строку в массив ($asRow перезапишется, если уже является массивом)

		Switch $i
			Case Not $aFile[0]
				readLine($aFile[$i], $asRow)
			Case Else
				readLine($aFile[$i], $asRow, Default, True)
		EndSwitch
		If @error Then ContinueLoop

		; обходим массив экстракторов и пишем в файл найденные данные
		procRow($aiCols, $asRow, $bWasSaveEvent, $iRowMatching)
		If @error Then
			MsgBox($MB_ICONERROR, $g_sWinTitle, "Возникла проблема при записи в папку. Рекомендуется удалить созданное содержимое и запустить процесс нарезки на файлы заново", 0, $hGUI)
			Return SetError(5)
		EndIf

		;Sleep(10)
	Next

	; не был сохранён ни 1 файл
	If Not $bWasSaveEvent Then
		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Не было создано ни одного файла." & @CRLF & 'Возможные причины: CSV не является корректным экспортом Лягушки или заданные экстракторы не содержат контента (либо он был отфильтрован по ограничению в символах).', 0, $hGUI)
		Return SetError(6)
	EndIf

	; сохранить массив сопоставления в файл
	writeArrMatching($iRowMatching)
	If @error Then
		MsgBox($MB_ICONERROR, $g_sWinTitle, "Не получилось сохранить таблицу сопоставления в файл." & @CRLF & @CRLF & "(код ошибки " & @error & ")", 0, $hGUI)
		Return SetError(7)
	EndIf
EndFunc   ;==>procCSV


; разобрать КСВ в массив
Func getFileData(ByRef $aFile)

	Local $hFile = FileOpen($g_sCSVpath)
	If @error Then Return SetError(1)

	Local $sFileData = FileRead($hFile)
	If @error Then Return SetError(2)

	FileClose($hFile)

	$aFile = StringSplit($sFileData, '"' & @CRLF & '"', $STR_ENTIRESPLIT)
	If @error Then Return SetError(3)

	; выпиливаем завершающую кавычку в последнем элементе массива
	$aFile[$aFile[0]] = StringTrimRight($aFile[$aFile[0]], 1)

EndFunc   ;==>getFileData


; (ре-)инициализация массива сопоставления имён файлов и урлов
Func initArrMatching($iCounter)

	; по размеру будет на 1 элемент меньше, но т.к. счётчик
	; исключен, при необходимости влезет всё
	Dim $g_aMatching[$iCounter][2]
	If @error Then Return SetError(1)

EndFunc   ;==>initArrMatching


; получаем порядковые номера колонок, данные из которых нужно получить
Func getColsCounts(ByRef $aInputs, ByRef $aFile, ByRef $aiCols)

	; разобрать строку заголовков (1-я) в массив
	; (каждый элемент которого представляет значение между разделителями)
	Local $aExtractorNames
	readLine($aFile[1], $aExtractorNames, False)

	; очищаем нумерацию экстракторов;
	; хотя потом придётся опять проходить массив названий, это более экономично,
	; т.к. иначе StringRegExpReplace пришлось бы выполнять для каждого названия
	; при проверке каждого экстрактора, заданного пользователем;

	; пропускаем первые 3 элемента, т.к. не являются экстракторами;
	; предварительно удалить эти элементы ни в коем случае нельзя,
	; т.к. тогда не будут совпадать по порядковым номерам массив
	; с номерами колонок и массив с разобранным рядом

	For $i = 4 To $aExtractorNames[0]
		$aExtractorNames[$i] = StringRegExpReplace($aExtractorNames[$i], '\s[0-9]{1,}$', '')
	Next

	;обходим каждое значение, указанное пользователем, против заголовков
	Local $sInput
	For $i = 1 To $aInputs[0]
		$sInput = $aInputs[$i]

		; если в качестве значения экстрактора указано название
		; (строка преобразуется к 0)
		If Number($sInput) = 0 Then

			fillCountsByName($sInput, $aExtractorNames, $aiCols)
			If @error Then Return SetError(1)

			;если в качестве значения экстрактора указан порядковый номер
		Else
			Select
				Case $i > 1 And $i < $aInputs[0]
					$sInput = getExtractorNamebySN($sInput, $aExtractorNames)
				Case $i = 1
					$sInput = getExtractorNamebySN($sInput, $aExtractorNames, True)
				Case $i = $aInputs[0]
					$sInput = getExtractorNamebySN($sInput, $aExtractorNames, Default, True)
			EndSelect
			If @error Then Return SetError(2)

			fillCountsByName($sInput, $aExtractorNames, $aiCols)
			If @error Then Return SetError(3)
		EndIf
	Next

	; добавляем счётчик элементов; просто UBound (без минус 1), т.к. при выполнении ubound
	; не учитывается элемент, который добавляется данной командой
	_ArrayInsert($aiCols, 0, UBound($aiCols))

EndFunc   ;==>getColsCounts


; формируем массив элементов из строки КСВ;
Func readLine($sString, ByRef $asRow, $bStrValidation = True, $bClearStatic = False)

	; регулярка урла в файле лягушки (PCRE не понимает диапазоны кириллицы: а-яё);
	; предположительно Static экономит память, т.к. переменная
	; не перезадаётся при разборе каждой строки
	Static Local $sPattern = '^' & 'https?:\/\/(www\.)?[a-z0-9абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ!\#\$%&\(\)\*\+,-\.\/:;<=>\?@\[\\\]\^_\{\|\}~]{3,1999}' & '","200","OK","'

	If $bStrValidation Then
		; строка некорректна и не подлежит последующему разбору
		If Not StringRegExp($sString, $sPattern) Then Return SetError(1)
	EndIf

	; для инфы: если в тексте экстрактора идёт что-то, что попадает под выражение,
	; строка будет разобрана неверно; хотя это маловероятно, но всё же
	$asRow = StringSplit($sString, '","', $STR_ENTIRESPLIT)

	If $bClearStatic Then $sPattern = ''

EndFunc   ;==>readLine


; наполнение массива порядковых номеров колонок по названию экстрактора
Func fillCountsByName($sString, ByRef $aExtractorNames, ByRef $aiCols)

	; т.к. массив заголовков содержит названия экстракторов без добавленных
	; лягушкой номеров, записываем номера всех столбцов, где встречается соотв. название

	; пропускаем первые 3 элемента, т.к. не являются экстракторами;
	; $bFound - переменная статуса, которая свидетельствует о том,
	; что как минимум один столбец был найден

	Local $bFound
	For $i = 4 To $aExtractorNames[0]
		If $sString = $aExtractorNames[$i] Then
			_ArrayAdd($aiCols, $i)
			If Not $bFound Then $bFound = True
		EndIf
	Next

	; не получилось найти экстрактор с заданным именем; также, это может быть
	; следствием некорректного вбития юзером значения(-й) в поле экстракторов
	If Not $bFound Then SetError(1)
EndFunc   ;==>fillCountsByName


; для получения названия экстрактора по порядковому номеру, указанному пользователем
Func getExtractorNamebySN($sNum, ByRef $aExtractorNames, $bPrepTempArr = False, $bClearStatic = False)

	; на тот случай, если получилось дробное значение из-за некорректного
	; значения от пользователя
	Local $iNum = Floor(Number($sNum))

	; порядковый номер экстрактора на самом деле на 3 больше,
	; т.к. первые 3 столбца не являются экстракторами
	$iNum += 3

	; т.к. будем производить операции по изменению массива заголовков,
	; нужно сделать его копию, т.к. оригинальный массив не должен измениться
	Static Local $aTemp = $aExtractorNames

	If $bPrepTempArr Then

		; если этого не сделать, то счётчик элементов массива до уникализации останется,
		; и сдвинет счётчик элементов на 1; для удобства надо от него избавиться
		_ArrayDelete($aTemp, 0)

		; удалив все повторения из массива заголовков, получим возможность
		; узнать название конкретного экстрактора по его порядковому номеру
		$aTemp = _ArrayUnique($aTemp)
	EndIf

	; пропускаем первые 3 элемента, т.к. не являются экстракторами
	For $i = 4 To $aTemp[0]

		; номер, указанный юзером совпал с порядковым номером экстрактора,
		; возвращаем название экстрактора
		If $iNum = $i Then Return $aTemp[$i]
	Next

	If $bClearStatic Then $aTemp = 0

	; не получилось найти экстрактор по заданному порядковому номеру; также, это может быть
	; следствием некорректного вбития юзером значения(-й) в поле экстракторов
	SetError(1)
EndFunc   ;==>getExtractorNamebySN


; обходим строку по порядковым номерам экстракторов и выдираем соотв. значения
Func procRow( _
		ByRef $aiCols, ByRef $asRow, ByRef $bWasSaveEvent, ByRef $iRowMatching _
		)

	Local $iCol
	Local $sData = ""
	Local $bFirstContentAdded = False

	For $i = 1 To $aiCols[0]
		$iCol = $aiCols[$i]

		; если строка была разобрана некорректно, предотвращаем ошибку
		; выхода за границы массива
		If $iCol > $asRow[0] Then ExitLoop

		; экстрактор под заданным порядковым номером не содержит ничего полезного;
		; можно было бы обойтись, чтобы увеличить скорость, но это чтобы
		; не дописывать лишние пробелы в файл (важнее)
		If Not StringStripWS($asRow[$iCol], 8) Then ContinueLoop

		If $bFirstContentAdded Then
			$sData &= " " & $asRow[$iCol]
		Else
			$sData = $asRow[$iCol]
			$bFirstContentAdded = True
		EndIf
	Next

	; если не было выдрано никакого контента, или
	; длина контента менее заданного порога в символах
	If ($sData = "") Or _
			($g_iCharsLimitOn And (StringLen($sData) < $g_iCharsLimit)) _
			Then
		Return
	EndIf

	; в массиве 0-м элементом будет имя файла, а первым - полный путь к файлу
	Local $aFile = getRandomFile()

	saveTxt($aFile[1], $sData)
	If @error Then Return SetError(1)

	; первый параметр - имя файла, 2-й - урл
	updateArrMatching($aFile[0], $asRow[1], $iRowMatching)

	; переменная статуса о том, был ли сохранён как минимум 1 файл
	If Not $bWasSaveEvent Then $bWasSaveEvent = True

EndFunc   ;==>procRow


Func getRandomFile()

	Local $sFileName, $sFilePath
	Do
		$sFileName = Random(00000, 99999, 1) & "--cbfeh--.txt"
		$sFilePath = $g_sDirPath & "\" & $sFileName
	Until Not FileExists($sFilePath)

	Local $aArray[2] = [$sFileName, $sFilePath]
	Return $aArray
EndFunc   ;==>getRandomFile


; для записи содержимого экстракторов строки в текстовый файл
Func saveTxt($sFilePath, ByRef $sData)

	; 512 это анси - только в 1251 работает локальная проверка etxt (на случай, если понадобится)
	Local $hFile = FileOpen($sFilePath, 2 + 512)
	If $hFile = -1 Then Return SetError(1)

	If Not FileWrite($hFile, $sData) Then Return SetError(2)
	FileClose($hFile)
EndFunc   ;==>saveTxt


; обязательно с ByRef, т.к. изменения нужны во вне
Func updateArrMatching($sFileName, $sURL, ByRef $iRowMatching)

	; пишем данные в массив сопоставления
	$g_aMatching[$iRowMatching][0] = $sFileName
	$g_aMatching[$iRowMatching][1] = $sURL
	$iRowMatching += 1
EndFunc   ;==>updateArrMatching


; запись массива сопоставления в файл
Func writeArrMatching($iRowMatching)

	ReDim $g_aMatching[$iRowMatching][2]
	If @error Then Return SetError(1)

	; перезапись файла, режим утф8 с бом
	Local $hFile = FileOpen( _
			$g_sDirPath & "\" & $g_sFileMatchingName, _
			2 + 128 _
			)
	If $hFile = -1 Then Return SetError(2)

	_FileWriteFromArray($hFile, $g_aMatching, Default, Default, '|')
	If @error Then Return SetError(3)

	FileClose($hFile)
EndFunc   ;==>writeArrMatching

#EndRegion ; разрезка на файлы --------


#Region отсев проверенных файлов ----------

Func splitFiles($hGUI)

	Local $aHtml
	readEtxtReports($aHtml, $hGUI, False)
	If @error Then Return SetError(1)

	; массив для включения путей файлов на удаление,
	; без счётчика, т.к. он позже может быть уникализирован,
	; что и добавит счётчик... а если не будет уникализирован, то что?
	Local $aDelList[0]

	If $g_iHtmlRadio = 2 Then
		; если радиобатон выставлен в значение удалить все файлы отчётов,
		; то нет смысла составлять массив файлов на удаление
		$aDelList = $aHtml
	Else
		findObsoleteFiles($aHtml, $aDelList)
		If $g_iHtmlRadio = 1 Then

			; если радиобатон выставлен в значение удалить последний, то надо ещё найти последний файл
			Local $sLatestFilePath = getLastHtml()

			;MsgBox(0, "" , $sLatestFilePath)
			;ConsoleWrite($sFilePath = 0 & @CRLF)

			If $sLatestFilePath Then _ArrayAdd($aDelList, $sLatestFilePath)

			; массив может содержать дубли после данной операции (например, файл undefined
			; был изменён последним, попал в массив, но он уже раньше попал в массив
			; на этапе чистки от мусора);
			; если массив пустой, то операция уникализации его испортит
			If Not UBound($aDelList) = 0 Then $aDelList = _ArrayUnique($aDelList)
		Else
			; добавить счётчик в массив, чтобы работать с массивом в одном формате
			; (уникализация массива добавляет счётчик),
			; не надо отнимать 1, т.к. после операции размер увеличивается на 1,
			; и счётчик содержит правильное значение
			_ArrayInsert($aDelList, 0, UBound($aDelList))
		EndIf
	EndIf

	; если есть что удалить
	If UBound($aDelList) >= 2 Then
		delObsoleteReports($aDelList, $hGUI)
		If @error Then Return SetError(2)
	EndIf

	; если были удалены все файлы отчётов, то нет смысла разбирать,
	; какие файлы источники надо исключить из работы (т.к. не будет исключений)
	If $g_iHtmlRadio = 2 Then Return

	procSources($aHtml, $aDelList, $hGUI)
	If @error Then Return SetError(3)

EndFunc   ;==>splitFiles


; для поиска отчётов eTXT, которые должны быть удалены, вне зависимости от настроек
Func findObsoleteFiles(ByRef $aHtml, ByRef $aDelList)

	For $i = 1 To $aHtml[0]

		; для скорости и экономии ресурсов проверяем сначала приблизительно, а потом уже регуляркой
		If StringInStr($aHtml[$i], "_undef.html") And _
				StringRegExp($aHtml[$i], '.*?--cbfeh--\.txt_[0-9-]{8}_[0-9-]{10}_undef.html$') Then
			_ArrayAdd($aDelList, $aHtml[$i])

		ElseIf contentIsObsolete($aHtml[$i]) Then
			_ArrayAdd($aDelList, $aHtml[$i])
		EndIf
	Next
EndFunc   ;==>findObsoleteFiles


; проверка файла отчёта eTXT на предмет указания на отсутствие исходного текста
Func contentIsObsolete($sFilePath)

	Local $sContent = _readFile($sFilePath)

	; (?s) внутри групп для того, чтобы точка совпадала и с переносом строки в конкретных случаях
	Local $sPattern = '((?s).*?)<th>Исходный текст</th>((?s).*?)<div.*?>Информация не доступна</div>'

	Return StringRegExp($sContent, $sPattern) = 1 ? True : False
EndFunc   ;==>contentIsObsolete


; для поиска последнего файла отчёта етхт
Func getLastHtml()
	; читать второй раз в массив почти то же самое не очень правильно, однако,
	; если последний изменённый файл будет undefined, то не удалится нужный файл
	Local $aHtmlNoUndefined
	readEtxtReports($aHtmlNoUndefined, Default, Default, True)

	; нужно удалить счётчик массива, иначе результаты будут некорректны
	_ArrayDelete($aHtmlNoUndefined, 0)

	; последний изменённый файл;
	; здесь возможно попадание в массив элемента, который уже есть там,
	; но борьба с этим видится более затратной, чем последующая уникализация
	; массива
	Return _FilesTimesFromArray($aHtmlNoUndefined, Default, Default, False)
EndFunc   ;==>getLastHtml


; удалить отобранные файлы отчёта етхт
Func delObsoleteReports(ByRef $aFileList, $hGUI)
	For $i = 1 To $aFileList[0]
		If Not FileDelete($aFileList[$i]) Then
			MsgBox($MB_ICONERROR, $g_sWinTitle, "Не удалось удалить файл:" _
					 & @CRLF & $aFileList[$i], 0, $hGUI)
			Return SetError(1)
		EndIf
	Next
EndFunc   ;==>delObsoleteReports


; для работы с файлами исходниками, которые были проверены etxt
Func procSources(ByRef $aHtml, ByRef $aDelList, $hGUI)

	Local $aSources = getSources($aHtml, $aDelList)
	; не найдено файлов для упаковки/удаления, завершаем без ошибки
	If @error Then Return

	; упаковать исходники, потом удалить
	zipAndDelSources($aSources, $hGUI)
	If @error Then Return SetError(1)
EndFunc   ;==>procSources


; получить массив с именами файлов-исходников
Func getSources(ByRef $aHtml, ByRef $aDelList)

	; создаём копии и удаляем счётчики массивов перед сравнением
	Local $aHtmlCopy = $aHtml, $aDelListCopy = $aDelList
	_ArrayDelete($aHtmlCopy, 0)
	_ArrayDelete($aDelListCopy, 0)

	Local $aUniq1, $aUniq2, $aMatches
	_ArraysCompare(1, $aHtmlCopy, $aDelListCopy, $aUniq1, $aUniq2, $aMatches)
	$aHtmlCopy = 0
	$aDelListCopy = 0
	Local $aValidHtmls = $aUniq1
	$aUniq1 = 0

	Local $iUbound = UBound($aValidHtmls)

	; массив пуст, значит нет файлов исходников для последующей упаковки
	If $iUbound = 0 Then Return SetError(1)

	Local $aRet[$iUbound]
	; получить из названий html названия исходников
	For $i = 0 To $iUbound - 1
		$aRet[$i] = StringRegExpReplace($aValidHtmls[$i], '(--cbfeh--\.txt)_.*', '\1')
	Next

	; добавляем счётчик
	_ArrayInsert($aRet, 0, $iUbound)
	Return $aRet
EndFunc   ;==>getSources


Func zipAndDelSources(ByRef $aSources, $hGUI)

	Local $bAtLeastOneSrcExists = False
	Local $sZipPath

	For $i = 1 To $aSources[0]

		; проще проверять каждый файл на существование во время единственного обхода массива,
		; чем перебирать массив дважды (первый раз - актуализация существующих файлов)
		If FileExists($aSources[$i]) Then

			; не создавали массив ранее, чтобы не было на выходе пустого массива
			; (в случае, если не будет файлов для упаковки)
			If Not $bAtLeastOneSrcExists Then
				$sZipPath = createZip('sources_', $aSources[$i])
				If @error Then
					MsgBox($MB_ICONERROR, $g_sWinTitle, "Не удалось создать зип архив", 0, $hGUI)
					Return SetError(1)
				EndIf
				$bAtLeastOneSrcExists = True
			EndIf

			_Zip_AddItem($sZipPath, $aSources[$i])
			If @error Then
				MsgBox($MB_ICONERROR, $g_sWinTitle, "Не удалось добавить файл" & @CRLF _
						 & $aSources[$i] & @CRLF & 'в архив' & @CRLF & $sZipPath, 0, $hGUI)
				Return SetError(2)
			EndIf
			; без проверки на ошибки, т.к. это текстовый файл, не должно быть проблем
			FileDelete($aSources[$i])
		EndIf
	Next

	;If Not $bAtLeastOneSrcExists Then
	;	MsgBox($MB_ICONINFORMATION, $g_sWinTitle, "Не найдено новых файлов для упаковки", 0, $hGUI)
	;	Return SetError(3)
	;EndIf
EndFunc   ;==>zipAndDelSources


Func createZip($sFrag, $sSomeSourceFilePath)

	Local $sZipPath = getZipPath($sFrag, $sSomeSourceFilePath)
	_Zip_Create($sZipPath)
	If @error Then Return SetError(1)
	Return $sZipPath

EndFunc   ;==>createZip


; получить полный путь к создаваемому архиву;
; если это не первая чистка/остановка работы етхт, то могут быть ранее созданные
; архивы с исходниками, поэтому надо их сохранить в неприкосновенности
Func getZipPath($sFrag, $sSomeSourceFilePath)

	; получаем путь к папке
	Local $sPath, $sDirPath = _GetDirPathFromFilePath($sSomeSourceFilePath)

	; если корневой каталог, надо убрать лидирующий слеш, чтобы использовать один формат
	; формирования имени файла архива
	$sDirPath = _RemoveTrailingBackslash($sDirPath)

	Local $i = 1
	Do
		$sPath = $sDirPath & '\' & $sFrag & '_' & $i & '.zip'
		$i += 1
	Until Not FileExists($sPath)
	Return $sPath

EndFunc   ;==>getZipPath

#EndRegion отсев проверенных файлов ----------


#Region ; формирование отчёта --------

Func formReport($hGUI)

	Local $aFiles
	readEtxtReports($aFiles, $hGUI)
	If @error Then Return SetError(1)

	; страховка на случай, если фех покорежит файлы отчётов напрасно
	zipEtxtResults($aFiles)

	; получаем массив сопоставления (имён файлов и урлов)
	getArrMatching()

	;переменная статуса для первоначального наполнения файла отчёта
	Local $bFirstWriteEventDone = False

	;файл отчёта будет создан позже, переменная укажет на файл с пом. ByRef;
	;это для того, чтобы не перезаписывать существующий одноименный файл,
	;если он есть и если среди выборки не будет ни одного корректного отчёта etxt
	Local $hFileReport

	For $i = 1 To $aFiles[0]

		procOneHTML($i, $aFiles[$i], $hFileReport, $bFirstWriteEventDone)
		If @error Then
			MsgBox($MB_ICONERROR, $g_sWinTitle, "Не удалось произвести запись в файл отчёта, либо в один из файлов от eTXT, попробуйте запустить создание отчёта повторно.", 0, $hGUI)
			Return SetError(2)
		EndIf

		; здесь нужно писать в текстовый проверенный файлик урл первой строкой в обертке <a> и делать пропуск строки
		; но тогда потребуется корректировка и чистки - удалять этот же урл из обработанных текстовых файликов
	Next

	If $bFirstWriteEventDone Then
		writeEndBlock($hFileReport)
		If @error Then
			MsgBox($MB_ICONERROR, $g_sWinTitle, "Не удалось произвести запись в файл отчёта, либо в один из файлов от eTXT, попробуйте запустить создание отчёта повторно.", 0, $hGUI)
			Return SetError(3)
		EndIf
	Else
		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Не найдено ни одного HTML файла корректного формата, создание отчёта отменено", 0, $hGUI)
		Return SetError(4)
	EndIf
EndFunc   ;==>formReport


Func zipEtxtResults(ByRef Const $aFiles)

	; берём путь из первого файла отчёта
	Local $sZipPath = createZip('etxt_reports', $aFiles[1])

	For $i = 1 To $aFiles[0]
		_Zip_AddItem($sZipPath, $aFiles[$i])
	Next
EndFunc   ;==>zipEtxtResults


Func getArrMatching()

	; если в файле хотя бы 1 лишняя пустая строка, тогда массив не считывается
	_FileReadToArray($g_sDirPath & "\" & $g_sFileMatchingName, _
			$g_aMatching, Default, '|')

	; проверку на ошибки не делаем, чтобы можно было использовать скрипт для
	; работы с любыми отчётам етхт

EndFunc   ;==>getArrMatching


;обработать один HTML файл отчёта
Func procOneHTML($iFileCount, $sFilePath, ByRef $hFileReport, ByRef $bFirstWriteEventDone)

	; пока открываем только на чтение
	Local $sContent = _readFile($sFilePath)

	;получаем имя файла и уникальность
	Local $sFileName, $sURL, $sUniq, $sUrlHtml
	getHTMLdata($sContent, $sFileName, $sURL, $sUniq, $sUrlHtml)

	;если файл некорректный, просто пропускаем его
	If @error Then Return

	; меняем имя файла на урл, чтобы в отчёте был урл
	changeHTML($sContent, $sFilePath, $sFileName, $sUrlHtml)
	If @error Then Return SetError(1)

	; если это первый файл для отчёта, то создаём отчёт и производим его
	; первоначальное наполнение
	If Not $bFirstWriteEventDone Then
		writeFirstBlock($hFileReport)
		If @error Then Return SetError(2)
		$bFirstWriteEventDone = True
	EndIf

	;пишем данные из файла в отчёт, добавляя ссылку на файл;
	writeWorkBlock($iFileCount, $sFilePath, $hFileReport, $sURL, $sUniq)
	If @error Then Return SetError(3)
EndFunc   ;==>procOneHTML


;для получения данных из одного файла-отчёта в формате HTML от etxt
Func getHTMLdata(ByRef $sContent, ByRef $sFileName, ByRef $sURL, ByRef $sUniq, ByRef $sUrlHtml)

	; проверяем файл на отсутствие проверяемого текста
	Local $iNoText = StringRegExp($sContent, "<th>Исходный текст<\/th>.*?<div.*?Информация не доступна<\/div>")
	If $iNoText Then Return SetError(1)

	; выдираем имя локального файла
	Local $aFile = StringRegExp($sContent, "<p id='start.*?>Операция поиска \#[0-9]{1,}\s{0,}\((.*?)\)<\/p>", 1)

	; отфильтровываем html, который не является файлом отчёта etxt
	If Not IsArray($aFile) Then Return SetError(2)

	; выдираем результат по уникальности от eTXT
	Local $aUniq = StringRegExp($sContent, "<span class='uniq'>(.*?)<\/span>", 1)

	; если это косячный файл отчёта (первый фрагмент кода найден, а этот - нет),
	; нужно завершить, чтобы не было ошибки выхода за границы массива
	; (даже без ошибки, всё равно не было бы результата в отчёте по данному файлу)
	If Not IsArray($aUniq) Then Return SetError(3)

	; можно было бы отсеять файл по ряду других условий, но это не сделано,
	; в том числе потому, чтобы была возможность использовать скрипт для формирования
	; отчёта по любым локальным файлам, а не только созданным данным скриптом

	; убедились, что файл подходящий, передаем переменные ByRef в родительскую ф-цию

	$sUniq = $aUniq[0]
	$sFileName = $aFile[0]
	$sURL = getUrlFromFile($aFile[0])

	$sUrlHtml = '<a href="' & $sURL & '" target="_blank">' & $sURL & '</a>'

	;добавляем пробелы в начале и в конце, чтобы урл в скобках
	;удобнее визуально воспринимался в отчёте
	$sUrlHtml = " " & $sUrlHtml & " "

EndFunc   ;==>getHTMLdata


; получить урл по названию файла, из массива сопоставления
Func getUrlFromFile($sFileName)

	; если это произвольные отчёты етхт, а не нарезка Феха
	If Not IsArray($g_aMatching) Then Return $sFileName

	For $i = 1 To $g_aMatching[0][0]
		If $sFileName = $g_aMatching[$i][0] Then Return $g_aMatching[$i][1]
	Next

	; совпадений не найдено, возвращаем имя файла,
	; чтобы была возможность использовать скрипт для формирования отчёта
	; не только из нарезки Феха
	Return $sFileName
EndFunc   ;==>getUrlFromFile


; для изменения имени файла на урл в файле отчёта etxt
Func changeHTML(ByRef $sContent, $sFilePath, $sFileName, $sURL)

	Local $hFile = FileOpen($sFilePath, $FO_OVERWRITE)
	If $hFile = -1 Then Return SetError(1)

	$sContent = StringReplace($sContent, $sFileName, $sURL)

	If Not FileWrite($hFile, $sContent) Then
		FileClose($hFile)
		Return SetError(2)
	Else
		FileClose($hFile)
	EndIf
EndFunc   ;==>changeHTML


;пишем первый кусок HTML в отчёт
Func writeFirstBlock(ByRef $hFile)
	$hFile = FileOpen($g_sHTMLfilePath, $FO_OVERWRITE + $FO_UTF8_NOBOM)
	If @error Then Return SetError(1)

	Local $aData[0]
	_ArrayAdd($aData, '<!DOCTYPE html>')
	_ArrayAdd($aData, '<html lang="ru">')
	_ArrayAdd($aData, '<head>')
	_ArrayAdd($aData, '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-flat-top.min.css">')
	_ArrayAdd($aData, '<style>.pace .pace-progress{background:#1aff1a}</style>')
	_ArrayAdd($aData, '<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>')
	_ArrayAdd($aData, '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">')
	_ArrayAdd($aData, '<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">')
	_ArrayAdd($aData, '<style>body{background:#595959}.wrapper{margin:0 auto;padding:40px;max-width:800px}#h1,thead,tfoot,.dataTables_info,.dataTables_wrapper{background:#fff}h1{font-size:1.5em}#h1{padding:10px 0 1px;text-align:center;color:#52527a}.dataTables_info{width:98%;padding-left:2%;padding-bottom:20px}.dt-buttons{padding:0 0 5px 20px}td a{text-decoration:none;color:#0000b3}td a:hover{transition:color .4s;color:#66f}td a:visited{color:#7a0099}td{font-size:1.2em}th,td{max-width:120px!important;word-wrap:break-word}td.centered{text-align:center}</style>')
	_ArrayAdd($aData, '<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>')
	_ArrayAdd($aData, '<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>')
	_ArrayAdd($aData, '<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>')
	_ArrayAdd($aData, '<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>')
	_ArrayAdd($aData, '<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>')
	_ArrayAdd($aData, '<meta charset="UTF-8">')
	_ArrayAdd($aData, '<title>Уникальность текстов по данным eTXT</title>')
	_ArrayAdd($aData, '</head>')
	_ArrayAdd($aData, '<body>')
	_ArrayAdd($aData, '<div class="wrapper">')
	_ArrayAdd($aData, '<div id="h1"><h1>Уникальность текстов по данным eTXT</h1></div>')
	_ArrayAdd($aData, '<table id="table" class="display">')
	_ArrayAdd($aData, '<thead>')
	_ArrayAdd($aData, '<tr>')
	_ArrayAdd($aData, '<th>№</th>')
	_ArrayAdd($aData, '<th>URL (файл)</th>')
	_ArrayAdd($aData, '<th>Уникальность</th>')
	_ArrayAdd($aData, '</tr>')
	_ArrayAdd($aData, '</thead>')
	_ArrayAdd($aData, '<tbody>')

	Local $sData = _ArrayToString($aData, @CRLF)
	If @error Then Return SetError(1, 0, 0)

	If Not FileWrite($hFile, $sData) Then
		FileClose($hFile)
		Return SetError(2)
	EndIf
EndFunc   ;==>writeFirstBlock


;пишем в отчёт данные по одному файлу-отчёту etxt
Func writeWorkBlock($iEtxtReportCount, $sEtxtReportFilePath, $hFileReport, $sURL, $sUniq)

	Local $aData[0]
	_ArrayAdd($aData, '<tr>')
	_ArrayAdd($aData, '<td>' & $iEtxtReportCount & '</td>')
	;ссылка на файл, а в качестве анкора - урл
	_ArrayAdd($aData, '<td><a href="' & $sEtxtReportFilePath & '" target="_blank">' & $sURL & '</a></td>')
	_ArrayAdd($aData, '<td class="centered">' & $sUniq & '</td>')
	_ArrayAdd($aData, '</tr>')

	Local $sData = _ArrayToString($aData, @CRLF)
	If @error Then Return SetError(1)

	If Not FileWrite($hFileReport, $sData) Then
		FileClose($hFileReport)
		Return SetError(2)
	EndIf
EndFunc   ;==>writeWorkBlock


;пишем закрывающий блок в HTML
Func writeEndBlock($hFile)

	Local $aData[0]
	_ArrayAdd($aData, '</tbody>')
	_ArrayAdd($aData, '<tfoot>')
	_ArrayAdd($aData, '<tr>')
	_ArrayAdd($aData, '<th>№</th>')
	_ArrayAdd($aData, '<th>URL (файл)</th>')
	_ArrayAdd($aData, '<th>Уникальность</th>')
	_ArrayAdd($aData, '</tr>')
	_ArrayAdd($aData, '</tfoot>')
	_ArrayAdd($aData, '</table>')
	_ArrayAdd($aData, '</div>')
	_ArrayAdd($aData, '<script>$(document).ready(function(){$("#table").DataTable({paging:!1,searching:!1,language:{processing:"Подождите...",search:"Поиск:",lengthMenu:"Показать _MENU_ записей",info:"Записи с _START_ до _END_ из _TOTAL_ записей",infoEmpty:"Записи с 0 до 0 из 0 записей",infoFiltered:"(отфильтровано из _MAX_ записей)",infoPostFix:"",loadingRecords:"Загрузка записей...",zeroRecords:"Записи отсутствуют.",emptyTable:"В таблице отсутствуют данные",paginate:{first:"Первая",previous:"Предыдущая",next:"Следующая",last:"Последняя"},aria:{sortAscending:": активировать для сортировки столбца по возрастанию",sortDescending:": активировать для сортировки столбца по убыванию"},buttons:{copyTitle:"Скопировано",copySuccess:{_:"записей: %d"}}},dom:"Bfrtip",buttons:["copy","csv","excel"],columnDefs:[{width:"15px",targets:0},{width:"35px",targets:2}]})});</script>')
	_ArrayAdd($aData, '</body>')
	_ArrayAdd($aData, '</html>')

	Local $sData = _ArrayToString($aData, @CRLF)

	If Not FileWrite($hFile, $sData) Then
		FileClose($hFile)
		Return SetError(1)
	Else
		FileClose($hFile)
	EndIf
EndFunc   ;==>writeEndBlock

#EndRegion ; формирование отчёта --------
