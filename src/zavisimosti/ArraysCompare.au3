#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once
#include <StringConstants.au3> ; для наглядности флагов при вырезании пробелов

#Region сравнение массивов ----------------

; сравнение двух одномерных массивов, на основе https://www.autoitscript.com/forum/topic/164728-compare-2-arrays-with-3-arrays-as-a-result/?tab=comments#comment-1201986
;
;  $iMode:
;	0 - все результаты
; 	1 - уникальное из массива 1
; 	2 - уникальное из массива 2
; 	3 - совпадения
;
; в режимах 1/2/3 нельзя передавать в качестве мусорных массивов значения (из-за ByRef),
; надо задать во вне произвольные переменные
;
; удаление пробелов в массиве, $iStripSpaces:
;   0 - не трогать пробелы (для точного сравнения)
;   1 - вырезать пробелы с конца строк
;   2 - вырезать пробелы с начала строк и с конца

Func _ArraysCompare( _
		$iMode, _
		ByRef Const $aArray1, _
		ByRef Const $aArray2, _
		ByRef $aUnique1, _
		ByRef $aUnique2, _
		ByRef $aMatches, _
		$iStripSpaces = 0 _
		)

	If $iStripSpaces = Default Then $iStripSpaces = 0

	Local $iFlagWS = __AC_GetFlagWS($iStripSpaces) ; режим работы с пробелами

	Local $oArray1 = __AC_MakeDictionary()
	Local $oArray2 = __AC_MakeDictionary()
	Local $oArrayMatches = __AC_MakeDictionary()

	__AC_FillDictionary($aArray1, $oArray1, $iFlagWS)
	__AC_FillDictionary($aArray2, $oArray2, $iFlagWS)

	For $sKey In $oArray1

		If $oArray2.Exists($sKey) Then ; найден дубль

			Switch $iMode

				Case 0, 1

					$oArray1.Remove($sKey)
					ContinueCase

				Case 0, 2

					$oArray2.Remove($sKey)
					ContinueCase

				Case 0, 3

					$oArrayMatches.Item($sKey)
			EndSwitch
		EndIf
	Next

	Switch $iMode

		Case 0, 1

			$aUnique1 = $oArray1.Keys()
			ContinueCase

		Case 0, 2

			$aUnique2 = $oArray2.Keys()
			ContinueCase

		Case 0, 3

			$aMatches = $oArrayMatches.Keys()
	EndSwitch
EndFunc   ;==>_ArraysCompare


Func __AC_MakeDictionary()

	Return ObjCreate("Scripting.Dictionary")

EndFunc   ;==>__AC_MakeDictionary


Func __AC_GetFlagWS($iStripSpaces)

	If Not $iStripSpaces Then Return 0

	Local $iFlagWS = $STR_STRIPTRAILING

	If $iStripSpaces = 2 Then $iFlagWS += $STR_STRIPLEADING

	Return $iFlagWS

EndFunc   ;==>__AC_GetFlagWS


Func __AC_FillDictionary(ByRef Const $aArray, ByRef $oArray, $iFlagWS)

	For $sElement In $aArray

		__AC_ProcessWS($sElement, $iFlagWS)

		$oArray.Item($sElement)
	Next
EndFunc   ;==>__AC_FillDictionary


Func __AC_ProcessWS(ByRef $sString, $iFlagWS)

	If Not $iFlagWS Then Return

	$sString = StringStripWS($sString, $iFlagWS)

	; если пробелом является &nbsp; (160) то StringStripWS не удалит
	; https://stackoverflow.com/a/29907712

	; проверка первых символов, т.к. это должно быть быстрее, чем сразу дёргать регулярку;
	; хотя, возможно, надо проверять регуляркой на любой невидимый пробел, а не только 160
	; (есть ли такие ещё?)
	; https://www.regular-expressions.info/unicode.html

	Local $sPatternSpace = '\p{Zs}{1,}'

	If AscW(StringRight($sString, 1)) = 160 Then

		$sString = StringRegExpReplace($sString, $sPatternSpace & '$', '')
	EndIf

	If Not ($iFlagWS = $STR_STRIPTRAILING) Then

		If AscW(StringLeft($sString, 1)) = 160 Then

			$sString = StringRegExpReplace($sString, '^' & $sPatternSpace, '')
		EndIf
	EndIf
EndFunc   ;==>__AC_ProcessWS

#EndRegion сравнение массивов ----------------
