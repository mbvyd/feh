#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once


Func _RemoveTrailingBackslash($sPath)

	Return StringRight($sPath, 1) = '\' ? StringTrimRight($sPath, 1) : $sPath

EndFunc   ;==>_RemoveTrailingBackslash


; из примера отсюда https://www.autoitscript.com/autoit3/docs/functions/FileSaveDialog.htm

Func _GetFileNameFromPath($sFilePath)

	Return StringTrimLeft($sFilePath, StringInStr($sFilePath, '\', Default, -1))

EndFunc   ;==>_GetFileNameFromPath

#cs
Func _GetFileNameFromPath2($sFilePath)
	Local $aFilePath = _StringExplode($sFilePath, '\')
	Return $aFilePath[UBound($aFilePath) - 1]
EndFunc
#ce


; получение пути папки из пути файла, либо получение
; пути папки на уровень выше из пути папки

Func _GetDirPathFromFilePath($sFilePath)

	Local $sBackslash = '\'

	; сдвиг строки с учётом обрезки лидирующего слеша
	Local $iSubtractor = 1

	; если файл в корневом каталоге, то лидирующий слеш нужно сохранить в пути папки
	StringReplace($sFilePath, $sBackslash, $sBackslash)
	If @extended = 1 Then $iSubtractor -= 1

	Return StringLeft($sFilePath, StringInStr($sFilePath, $sBackslash, Default, -1) - $iSubtractor)

EndFunc   ;==>_GetDirPathFromFilePath


#cs
; убрать имя файла из пути, чтобы получить путь к папке
Func _GetDirPathFromFilePath2($sFilePath)
	Local $sBackslash = '\'
	Local $aDirPath = _StringExplode($sFilePath, $sBackslash, -1)
	Local $sDirPath = _ArrayToString($aDirPath, $sBackslash)
	; если два символа, то это корневой каталог диска (c: , d: и тд), поэтому дописываем обратный слэш
	Return StringLen($sDirPath) <> 2 ? $sDirPath : $sDirPath & $sBackslash

EndFunc   ;==>_GetDirPathFromFilePath2


; вроде хуже, чем две предыдущие ф-ции, т.к. выполняется наибольшее число ф-ций
; (но это не показатель, т.к. они могут быть проще/быстрее)
Func _GetDirPathFromFilePath3($sFilePath)

	Local $sDirPath = StringLeft($sFilePath, StringInStr($sFilePath, '\', Default, -1))
	; если три символа, то это корневой каталог диска (c:\ , d:\ и тд),
	; поэтому возвращаем без обрезки обратного слэша
	Return StringLen($sDirPath) <> 3 ? StringTrimRight($sDirPath, 1) : $sDirPath

EndFunc   ;==>_GetDirPathFromFilePath3
#ce


; https://www.autoitscript.com/forum/topic/151918-_stringregexpmetacharacters-retain-special-characters-for-use-in-a-regular-expression/

Func _StringRegExpMetaCharacters($sString)

	Return StringRegExpReplace($sString, '([].|*?+(){}^$\\[])', '\\\1')

EndFunc   ;==>_StringRegExpMetaCharacters


; для выдирания домена из урла

Func _GetDomainFromUrl($sUrl)

	Return StringRegExpReplace($sUrl, '(?i)(https?://)?(www\.)?(.*?)(/.*|$)', '\3')

EndFunc   ;==>_GetDomainFromUrl


; для вырезки домена из урла

Func _GetRelUrl($sUrl)

	Local $sRelUrl = StringRegExpReplace($sUrl, '(?i)(https?://)?(www\.)?.*?\..*?(/.*|$)', '\3')

	Return $sRelUrl = '' ? '/' : $sRelUrl

EndFunc   ;==>_GetRelUrl


Func _StringNotEmpty($sString)

	Return StringLen(StringStripWS($sString, 8)) > 0 ? True : False

EndFunc   ;==>_StringNotEmpty
